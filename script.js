const gameContainer = document.getElementById("game");

const COLORS = [
  "red",
  "blue",
  "green",
  "orange",
  "purple",
  "red",
  "blue",
  "green",
  "orange",
  "purple",
];

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

let shuffledColors = shuffle(COLORS);

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(colorArray) {
  for (let color of colorArray) {
    
    // create a new div
    const newDiv = document.createElement("div");

    // give it a class attribute for the value we are looping over
    newDiv.classList.add(color);

    // give it a data attribute of the color of the card
    newDiv.setAttribute("data-card-color", color);

    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick);

    // append the div to the element with an id of game
    gameContainer.append(newDiv);
  }
}

var hasFlipped = false;
var lockBoard = false;
var card1, card2;
let sameColorCounter = 0;

// TODO: Implement this function!
function handleCardClick(event) {
  if(lockBoard) return;

  let currentCard = event.target;
  if(currentCard === card1) return;
  currentCard.style.backgroundColor = currentCard.dataset.cardColor;
  // you can use event.target to see which element was clicked
  console.log("you clicked", currentCard);

  if (!hasFlipped) {
    hasFlipped = true;
    card1 = currentCard;
  } else {
    hasFlipped = false;
    card2 = currentCard;
    isSameColor();
  }
}
function isSameColor(){
  if (card1.dataset.cardColor === card2.dataset.cardColor) {
    sameColorCounter++;
    removeEventListeneres();
  } else {
    revertColorToWhite();
  }
}

function removeEventListeneres() {
  card1.removeEventListener("click", handleCardClick);
  card2.removeEventListener("click", handleCardClick);

  if(sameColorCounter === (COLORS.length/2))
  {
    setTimeout(() => {
    alert("you won");
    },300);
  }

  resetBoards();

}

function revertColorToWhite() {
  lockBoard = true;
  setTimeout(() => {
    card1.style.backgroundColor = card2.style.backgroundColor = "white";
    resetBoards();
  }, 1000);
}

function resetBoards(){
  [hasFlipped, lockBoard] = [false, false];
  card1 = card2 = null;
 }
// when the DOM loads
createDivsForColors(shuffledColors);